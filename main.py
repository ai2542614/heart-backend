from fastapi import FastAPI
from pydantic import BaseModel
from fastapi.middleware.cors import CORSMiddleware
import joblib
import os
import numpy as np

app = FastAPI()
app.add_middleware(
    CORSMiddleware,
    allow_origins=["http://localhost:5173"],
    allow_credentials=True,
    allow_methods=["GET", "POST", "PUT", "DELETE"],
    allow_headers=["*"],
)

# Load the trained model
file_path = 'Heart_Failure_Prediction.pkl'
if os.path.exists(file_path):
    print(f'The file {file_path} exists.')
    # โหลดโมเดลจากไฟล์
    model = joblib.load(file_path)
else:
    print(f'The file {file_path} does not exist.')


class HeartFailureMeasurement(BaseModel):
    Age: int
    Sex: int
    ChestPainType: int
    RestingBP: int
    Cholesterol: int
    FastingBS: int
    RestingECG: int
    MaxHR: int
    ExerciseAngina: int
    Oldpeak: float
    ST_Slope: int

@app.post("/heart/predict")
async def Heart_Failure_Prediction(heart: HeartFailureMeasurement):
    try:
        data = [[heart.Age, heart.Sex, heart.ChestPainType, heart.RestingBP, heart.Cholesterol, heart.FastingBS, heart.RestingECG, heart.MaxHR, heart.ExerciseAngina, heart.Oldpeak, heart.ST_Slope]]
        prediction = model.predict(data)
        answer = ['normal', 'heart disease']
        predict_HeartFailure = answer[prediction[0]]

        return predict_HeartFailure
    except Exception as e:
        print("Error prediction:", e)
        return {"error": "Prediction failed"}